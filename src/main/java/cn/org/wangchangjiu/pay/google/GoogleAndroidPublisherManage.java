package cn.org.wangchangjiu.pay.google;

import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.auth.oauth2.GoogleCredentials;

import java.util.HashMap;
import java.util.Map;

/**
 * @Classname GoogleAndroidPublisherManage
 * @Description
 * @Date 2023/2/7 14:51
 * @author by wangchangjiu
 */
public class GoogleAndroidPublisherManage {

    private Map<String, AndroidPublisher> publisherMap = new HashMap<>();
    private Map<String, GoogleCredentials> credentialsMap = new HashMap<>();


    public void addPublisher(String androidOrPackage, AndroidPublisher androidPublisher){
        this.publisherMap.put(androidOrPackage, androidPublisher);
    }

    public AndroidPublisher getPublisher(String androidOrPackage){
       return this.publisherMap.get(androidOrPackage);
    }

    public void addCredentials(String androidOrPackage, GoogleCredentials credentials){
        this.credentialsMap.put(androidOrPackage, credentials);
    }

    public GoogleCredentials getCredentials(String androidOrPackage){
        return this.credentialsMap.get(androidOrPackage);
    }

}
