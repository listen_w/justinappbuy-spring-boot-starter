package cn.org.wangchangjiu.pay.google;

import cn.org.wangchangjiu.pay.common.VerifyResult;

/**
 * @Classname GoogleVerifyResult
 * @Description
 * @Date 2023/6/14 16:18
 * @Created by wangchangjiu
 */
public class GoogleVerifyResult implements VerifyResult<GoogleVerifyResult> {
    @Override
    public GoogleVerifyResult getData() {
        return this;
    }
}
