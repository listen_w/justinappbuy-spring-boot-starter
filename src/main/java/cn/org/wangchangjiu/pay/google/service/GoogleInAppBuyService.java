package cn.org.wangchangjiu.pay.google.service;

import cn.org.wangchangjiu.pay.common.*;
import cn.org.wangchangjiu.pay.common.dto.VipOrder;
import cn.org.wangchangjiu.pay.common.dto.VipRenewalOrder;
import cn.org.wangchangjiu.pay.common.enums.RenewalSignChannelEnum;
import cn.org.wangchangjiu.pay.common.enums.RenewalSignStatusEnum;
import cn.org.wangchangjiu.pay.common.request.GrantVipRequest;
import cn.org.wangchangjiu.pay.common.util.ConstantUtil;
import cn.org.wangchangjiu.pay.google.GoogleVerifyResult;
import cn.org.wangchangjiu.pay.google.request.GooglePayVerifyRequest;
import cn.org.wangchangjiu.pay.google.request.GoogleSubscribeCallback;
import com.alibaba.fastjson.JSON;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

/**
 * @Classname GooglePayVipService
 * @Description
 * @Date 2023/6/13 20:55
 * @Created by wangchangjiu
 */
public class GoogleInAppBuyService implements InAppBuyService<GooglePayVerifyRequest, GoogleSubscribeCallback> {

    private static final Logger log = LoggerFactory.getLogger(GoogleInAppBuyService.class);

    private InAppBuyDataRepository inAppBuyDataRepository;

    public GoogleInAppBuyService(InAppBuyDataRepository inAppBuyDataRepository){
        this.inAppBuyDataRepository = inAppBuyDataRepository;
    }

    @Override
    public VerifyResult verify(GooglePayVerifyRequest inAppVerifyRequest, Map<String, Object> params) {

        // 渠道
        String clientType = params.get("clientType") == null ? null : params.get("clientType").toString();
        String userId = params.get("userId") == null ? null : params.get("userId").toString();

        // 跳过本地校验
        boolean skipLockVerify = params.get("skipLockVerify") == null ? false : Boolean.valueOf(params.get("skipLockVerify").toString());
        GooglePayVerifyRequest.SigntureData signtureData = inAppVerifyRequest.getSigntureData();

        if(!skipLockVerify){
            // 本地 RSA 验签名
            String publicKey = inAppBuyDataRepository.getPackageAndPublicKey().get(clientType);
            log.info("clientType:{} , userId:{}, 包名：{}，公钥：{} 准备开始本地RSA校验", clientType, userId,  signtureData.getPackageName(), publicKey);
            if (!ConstantUtil.GooglePay.doCheck(inAppVerifyRequest.getSigntureContent(), inAppVerifyRequest.getSignture(), publicKey)) {
                log.info("服务端RSA校验未通过，参数：{}" , inAppVerifyRequest);
                throw new VerifyException(OrderVerifyEnum.RSA_VERIFY_FAIL.getCode(), OrderVerifyEnum.RSA_VERIFY_FAIL.getDesc());
            }
        }

        params.put("goodId", signtureData.getProductId());
        // 获取商品
        MarketVipGoodsVo marketVipGoodsVo = inAppBuyDataRepository.findMarketGoods(params);
        if (marketVipGoodsVo == null) {
            throw new VerifyException(OrderVerifyEnum.RSA_VERIFY_FAIL.getCode(), OrderVerifyEnum.RSA_VERIFY_FAIL.getDesc());
        }

        // 检查订单
        VipOrder checkOrder = inAppBuyDataRepository.findByThirdOrderSn(signtureData.getOrderId());
        log.info("客户端调用校验订单：{}，获取结果：{}", signtureData.getOrderId(), checkOrder);
        if(checkOrder != null) {
            if (checkOrder.needUpdateData()) {
               // log.info("用户：{} 更新订单数据：{}，{}", userId, JSON.toJSONString(checkOrder), JSON.toJSONString(marketVipGoodsVo));
               // inAppBuyDataRepository.googlePayUpdateOrderUserData(checkOrder, marketVipGoodsVo);
            }
            log.info("google 订阅回调订单：{} 已经生成，不需要再次处理", checkOrder.getId());
            throw new VerifyException(OrderVerifyEnum.RSA_VERIFY_FAIL.getCode(), OrderVerifyEnum.RSA_VERIFY_FAIL.getDesc());
        }


        String originInfo;
        GrantVipRequest grantVipRequest;
        VipOrder order;
        if (marketVipGoodsVo.isSubscribed()) {
            // 处理订阅商品，比如：VIP

            // 校验订单是否购买
            SubscriptionPurchase subscriptionPurchase = subscriptionsGoodsCheck(signtureData.getPackageName(), signtureData.getProductId(), signtureData.getPurchaseToken());
            log.info("verifyInapp方法 Google API 校验结果：{}", subscriptionPurchase);

            // 获取的数据中有 linkedPurchaseToken, 说明是重新订阅了，签约号（purchaseToken） 将被改变
            if(StringUtils.isNotEmpty(subscriptionPurchase.getLinkedPurchaseToken())) {

              /*  // 拿本次的 token 记录中查上次的 token 如果有记录 那么这是重复请求 不再处理
                VipRenewalOrder checkVipRenewalOrder = inAppBuyDataRepository.findVipRenewalOrderBySignNo(subscriptionPurchase.getLinkedPurchaseToken());
                log.info("通过 purchaseToken :{} 和 LinkedPurchaseToken ：{} 检查订单 ：{}", signtureData.getPurchaseToken(), subscriptionPurchase.getLinkedPurchaseToken(), checkVipRenewalOrder);
                if(checkVipRenewalOrder != null){
                    log.info("通过 purchaseToken :{} 和 LinkedPurchaseToken ：{} 存在续订，说明token已经替换了，是重复请求", signtureData.getPurchaseToken(), subscriptionPurchase.getLinkedPurchaseToken());
                    return null;
                }*/

                // 表示重新订阅 修改订阅
                VipRenewalOrder vipRenewalOrder = inAppBuyDataRepository.findVipRenewalOrderBySignNo(subscriptionPurchase.getLinkedPurchaseToken());
                if(vipRenewalOrder != null){
                    // 判断 订阅是否过期
                    boolean isSameDay = DateUtils.isSameDay(new Date(subscriptionPurchase.getExpiryTimeMillis()), vipRenewalOrder.getNextOrderTime()));
                    if(isSameDay){
                        log.info("收到新订阅，用户：{} google 订阅过期时间毫秒:{}，续订单过期时间：{}， 没有新加的日期，因此本次订阅无效，是用户取消后重新订阅的操作", vipRenewalOrder.getUserId(),
                                subscriptionPurchase.getExpiryTimeMillis(), vipRenewalOrder.getNextOrderTime());
                        vipRenewalOrder.setPurchaseToken(signtureData.getPurchaseToken());
                        vipRenewalOrder.setLinkedPurchaseToken(subscriptionPurchase.getLinkedPurchaseToken());
                        vipRenewalOrder.setSignNo(subscriptionPurchase.getObfuscatedExternalProfileId());
                        vipRenewalOrder.setNextOrderTime(new Date(subscriptionPurchase.getExpiryTimeMillis()));
                        inAppBuyDataRepository.saveVipRenewalOrder(vipRenewalOrder);
                        log.info("用户：{} 更新后的续订单：{}", vipRenewalOrder.getUserId(), JSON.toJSONString(vipRenewalOrder));
                        return null;
                    } else {
                        log.info("收到用户：{} 重新订阅，该订阅已经过期，google 订阅过期时间毫秒:{}，续订单过期时间：{}", vipRenewalOrder.getUserId(),
                                subscriptionPurchase.getExpiryTimeMillis(), vipRenewalOrder.getNextOrderTime());
                    }
                }
            }

            // 处理订单
            order = inAppBuyDataRepository.createOrderByGooglePay(inAppVerifyRequest, marketVipGoodsVo);

            originInfo = subscriptionPurchase == null ? null : JSON.toJSONString(subscriptionPurchase);
            if (ConstantUtil.GooglePay.checkSubscriptionPurchase(subscriptionPurchase)) {
                inAppBuyDataRepository.updateOrderFail(order.getId(), originInfo);
                // 订单未支付状态
                log.info("Google API 校验订单：{} 不是支付完成状态，结果:{}", order.getId(), originInfo);
                throw new VerifyException(OrderVerifyEnum.ORDER_UN_PAID.getCode(), OrderVerifyEnum.ORDER_UN_PAID.getDesc());
            }
            // 获取到订单价格
            order.setCountry(subscriptionPurchase.getCountryCode());
            // 使用google 订阅的用户不会导致充值错了用户的BUG  修改用户ID

            String orderUserId = userId;
            if(StringUtils.isNotEmpty(subscriptionPurchase.getObfuscatedExternalAccountId())){
                orderUserId = subscriptionPurchase.getObfuscatedExternalAccountId();
            } else {
                log.info("verifyInapp method thirdOrderSn:{},obfuscatedExternalAccountId is null, use getCurrentUserId:{}", order.getThirdOrderSn(), userId);
            }
            order.setUserId(orderUserId);
            inAppBuyDataRepository.updateOrder(order);

            // 创建续订单
            VipRenewalOrder vipRenewalOrder = inAppBuyDataRepository.createSubscribeOrder(order, subscriptionPurchase.getObfuscatedExternalProfileId(),
                    "", RenewalSignStatusEnum.SUCCESS, RenewalSignChannelEnum.GOOGLE,
                    JSON.toJSONString(subscriptionPurchase));

            updateRenewalOrder(vipRenewalOrder, subscriptionPurchase, signtureData.getPurchaseToken());

            // 计算发放天数
            Long diffTime = (subscriptionPurchase.getExpiryTimeMillis() - subscriptionPurchase.getStartTimeMillis());
            Long days = diffTime / 86400000;

            //   log.info("用户：{}，第0次订阅过期：{}，开始：{}，Google 发放毫秒：{}，天数：{}",userId, subscriptionPurchase.getExpiryTimeMillis() , subscriptionPurchase.getStartTimeMillis(), diffTime, days);
            if(days.intValue() <= 0 && genericPayProperties.isGoogleTestOpen()){
                log.info("google 发放天数：{}, user:{},orderId:{}, 证明非有效订阅", days.intValue(), order.getUserId(), order.getThirdOrderSn());
                return null;
            }
            grantVipRequest = Constant.buildGrantVipRequest(order, true);
        } else {
            order = inAppBuyDataRepository.createOrderByGooglePay(request, marketVipGoodsVo);
            // 会员 一次性购买
            String orderId = order.getId();
            originInfo = purchasesGoodsCheck(request, orderId, (info) -> vipOrderService.updateOrderFail(orderId, info));
            grantVipRequest = Constant.buildGrantVipRequest(order, true);
        }

        // 更新订单数据
        inAppBuyDataRepository.updateOrderPaid(order.getId(), originInfo);

        // 通知发放会员
        boolean grantUserVipSuccess = inAppBuyDataRepository.grantUserVip(grantVipRequest, order.getId(), marketVipGoodsVo, null);
        if (grantUserVipSuccess) {
            inAppBuyDataRepository.updateOrderDeliverSuccess(order);
        }

        return new GoogleVerifyResult();
    }

    private void handleSubscriptionPurchase() {




    }

    public SubscriptionPurchase subscriptionsGoodsCheck(String packageName, String productId, String purchaseToken) {
        SubscriptionPurchase purchaseSub;
        try {
            purchaseSub = androidPublisherManage.getPublisher(packageName).purchases()
                    .subscriptions().get(packageName, productId, purchaseToken).execute();
            log.info("调用 GoogleSDK 获取订单数据，参数：{}, {}, {}，返回结果：{}", packageName, productId, purchaseToken, JSON.toJSONString(purchaseSub));
        } catch (Exception ex) {
            log.error("调用GoogleSDK校验订单发生异常:{} ", ex);
            throw new RuntimeException("调用GoogleSDK校验订单发生异常");
        }
        return purchaseSub;
    }

    @Override
    public void subscribeNotify(GoogleSubscribeCallback googleSubscribeCallback) {

    }

    @Override
    public void applyRefund(VipOrder vipOrder) {

    }
}
