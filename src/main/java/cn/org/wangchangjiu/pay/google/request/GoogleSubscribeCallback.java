package cn.org.wangchangjiu.pay.google.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Classname GoogleSubscribeCallback
 * @Description
 * @Date 2022/11/8 14:05
 * @Created by wangchangjiu
 */
@Data
public class GoogleSubscribeCallback implements Serializable {

    private String subscription;

    /**
     *  消息数据
     */
    private Message message;

    @Data
    public static class Message {

        /**
         *  消息ID
         */
        private String messageId;

        /**
         *  发布时间
         */
        private String publishTime;

        /**
         *  base64 加密数据
         */
        private String data;

        /**
         *  解密实体
         */
        private DataDecode dataDecode;
    }


    /**
     *  接收的 data 数据使用 base64 解码
     */
    @Data
    public static class DataDecode {

        /**
         * 此通知的版本。最初，此值为“1.0”。此版本与其他版本字段不同。
          */
       private String version;

        /**
         *  与此通知相关的应用的软件包名称（例如“com.some.thing”）
         */
       private String packageName;

        /**
         *  事件发生的时间戳，以从公元纪年开始计算的毫秒数表示。
         */
       private long eventTimeMillis;

        /**
         *  如果此字段存在，则此通知与订阅相关，并且此字段包含与订阅相关的其他信息。
         *  请注意，此字段与 testNotification 和 oneTimeProductNotification 互斥。
         */
       private SubscriptionNotification subscriptionNotification;

        /**
         *  如果此字段存在，则此通知与测试发布相关。
         *  这些只通过 Google Play 管理中心发送。
         *  请注意，此字段与 subscriptionNotification 和 oneTimeProductNotification 互斥。
         */
       private TestNotification testNotification;
    }

    /**
     *  订阅通知
     */
    @Data
    public static class SubscriptionNotification {

        /**
         *  此通知的版本。最初，此值为“1.0”。
         *  此版本与其他版本字段不同。
         */
        private String version;

        /**
         * 订阅的 notificationType 可以具有以下值：
         * (1) SUBSCRIPTION_RECOVERED - 从帐号保留状态恢复了订阅。
         * (2) SUBSCRIPTION_RENEWED - 续订了处于活动状态的订阅。
         * (3) SUBSCRIPTION_CANCELED - 自愿或非自愿地取消了订阅。如果是自愿取消，在用户取消时发送。
         * (4) SUBSCRIPTION_PURCHASED - 购买了新的订阅。
         * (5) SUBSCRIPTION_ON_HOLD - 订阅已进入帐号保留状态（如果已启用）。
         * (6) SUBSCRIPTION_IN_GRACE_PERIOD - 订阅已进入宽限期（如果已启用）。
         * (7) SUBSCRIPTION_RESTARTED - 用户已通过 Play > 帐号 > 订阅恢复了订阅。订阅已取消，但在用户恢复时尚未到期。如需了解详情，请参阅 [恢复](/google/play/billing/subscriptions#restore)。
         * (8) SUBSCRIPTION_PRICE_CHANGE_CONFIRMED - 用户已成功确认订阅价格变动。
         * (9) SUBSCRIPTION_DEFERRED - 订阅的续订时间点已延期。
         * (10) SUBSCRIPTION_PAUSED - 订阅已暂停。
         * (11) SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED - 订阅暂停计划已更改。
         * (12) SUBSCRIPTION_REVOKED - 用户在到期时间之前已撤消订阅。
         * (13) SUBSCRIPTION_EXPIRED - 订阅已到期。
         */
        private int notificationType;

        /**
         *  购买订阅时向用户设备提供的令牌。
         */
        private String purchaseToken;

        /**
         *  所购买订阅的商品 ID（例如“monthly001”）。
         */
        private String subscriptionId;

        /**
         *  新的订阅
         * @return
         */
        public boolean newSubscription(){
            return this.getNotificationType() == 1 || this.getNotificationType() == 4
                    || this.getNotificationType() == 2 || this.getNotificationType() == 7;
        }

        /**
         *  订阅过期
         * @return
         */
        public boolean subscriptionExpired() {
            return this.getNotificationType() == 13;
        }


        /**
         *  取消订阅
         * @return
         */
        public boolean subscriptionCanceled() {
            return this.getNotificationType() == 3;
        }

        /**
         *  订阅撤销
         * @return
         */
        public boolean subscriptionRevoked(){
            return this.getNotificationType() == 12;
        }

        /**
         *  订阅暂停
         * @return
         */
        public boolean subscriptionPaused(){
            return this.getNotificationType() == 10 || this.getNotificationType() == 11;
        }

    }

    @Data
    public static class TestNotification {

        private String version;

    }

}
