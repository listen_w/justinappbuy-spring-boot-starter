package cn.org.wangchangjiu.pay.google.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

/**
 * @Classname GooglePayVerifyRequest
 * @Description
 * @Date 2022/8/2 10:46
 * @Created by wangchangjiu
 */
@Data
public class GooglePayVerifyRequest implements Serializable {

    /**
     *  签名数据
     */

    @NotNull
    private SigntureData signtureData;

    /**
     *  签名
     */
    @NotNull
    private String signture;

    /**
     *  签名数据
     */
    private String signtureContent;

    /**
     *  google_pay_h5、google_pay_pri
     */
    private String payMethod;

    /**
     *  商品ID
     */
    private Long goodsId;

    /**
     *  签名数据
     */
    @Data
    public static class SigntureData implements Serializable{

        /**
         *  订单 ID
         */
        @NotNull
        private String orderId;

        /**
         *  包名
         */
        @NotNull
        private String packageName;

        /**
         *  商品
         *
         */
        @NotNull
        private String productId;

        /**
         *  订单时间
         */
        @NotNull
        private Long purchaseTime;

        /**
         *  订单状态
         */
        @NotNull
        private Integer purchaseState;

        /**
         *  购买令牌
         */
        @NotNull
        private String purchaseToken;


        @Override
        public String toString() {
            return "SigntureData{" +
                    "orderId='" + orderId + '\'' +
                    ", packageName='" + packageName + '\'' +
                    ", productId='" + productId + '\'' +
                    ", purchaseTime=" + purchaseTime +
                    ", purchaseState=" + purchaseState +
                    ", purchaseToken='" + purchaseToken + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "GooglePayVerifyRequest{" +
                "signtureData=" + signtureData +
                ", signture='" + signture + '\'' +
                '}';
    }
}
