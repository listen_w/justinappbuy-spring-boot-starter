package cn.org.wangchangjiu.pay.common.enums;


public enum OrderRenewalStatusEnum {

    PROCESS(0),
    SUCCESS(1),
    FAILED(2),
    CANCEL(3),
    FAILURE(4),
    PAUSE(8)
    ;

    private final Integer code;

    OrderRenewalStatusEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public static String findNameByCode(Integer code){
        String statusName = "";
        if(OrderRenewalStatusEnum.PROCESS.getCode().equals(code)){
            statusName = "订阅处理中";
        } else if(OrderRenewalStatusEnum.SUCCESS.getCode().equals(code)){
            statusName = "订阅成功";
        } else if(OrderRenewalStatusEnum.FAILED.getCode().equals(code)){
            statusName = "订阅失败";
        } else if(OrderRenewalStatusEnum.CANCEL.getCode().equals(code)){
            statusName = "订阅取消";
        } else if(OrderRenewalStatusEnum.FAILURE.getCode().equals(code)){
            statusName = "订阅失效";
        } else if(OrderRenewalStatusEnum.PAUSE.getCode().equals(code)){
            statusName = "订阅暂停";
        }
        return statusName;
    }

}
