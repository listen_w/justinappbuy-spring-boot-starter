package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname AppleNotificationTypeEnum
 * @Description 苹果通知类型
 * @Date 2022/12/9 15:45
 * @Created by wangchangjiu
 */
public enum AppleNotificationTypeEnum {

    /**
     * 取消消息通知
     */
    CANCEL,
    /**
     * 指示客户对其订阅计划进行了更改，该更改将在下一次续订时生效。当前活动的计划不受影响。
     * 暂时不会出现此状态,因为我们订阅只上一个商品
     */
    DID_CHANGE_RENEWAL_PREF,
    /**
     * 修改了订阅状态
     * 订阅周期结束会触发;或者订阅周期结束前,重新订阅也可能会触发
     */
    DID_CHANGE_RENEWAL_STATUS,
    /**
     * 订阅失败
     */
    DID_FAIL_TO_RENEW,
    /**
     * 恢复:
     * 订阅失败后恢复或者通知失败会再次通知
     */
    DID_RECOVER,

    /**
     * 指示客户的订阅已成功自动续期为新的事务期
     */
    DID_RENEW,

    /**
     * 首次购买
     * 在用户首次购买订阅时发生。将latest_receipt作为令牌存储在服务器上，通过在App Store中验证它，随时验证用户的订阅状态。
     */
    INITIAL_BUY,

    /**
     * 手动续订成功,订阅周期结束,再次订阅会有此通知
     */
    INTERACTIVE_RENEWAL,

    /**
     * 表示App Store已开始要求客户同意您的App的订阅价格上涨。unified_receipt。Pending_renewal_infoobject中，
     * price_consent_status值为0，表示App Store正在征求客户的同意，但尚未收到客户的同意。除非用户同意新的价格，
     * 否则订阅不会自动续订。当客户同意提价时，系统将price_consent_status设置为1。使用verifyReceipt检查收据，
     * 查看更新后的价格同意状态。
     */
    PRICE_INCREASE_CONSENT,

    /**
     * 表示App Store已成功退款。cancellation_date_ms包含已退款事务的时间戳。
     * original_transaction_id和product_id标识原始事务和产品。cancellation_reason包含原因。
     */
    REFUND,

    /**
     * 指示成功自动续延过去未能续延的过期订阅。检查expires_date以确定下一次续订日期和时间。
     * 此通知已在sandbox环境中过时，并计划于2021年3月在生产中过时。更新现有代码，以依赖于DID_RECOVER通知类型。
     */
    RENEWAL;

}
