package cn.org.wangchangjiu.pay.common.dto;

import lombok.Data;

import java.util.Date;

/**
 * @Classname vipRenewalOrder
 * @Description 续约订单激励
 * @Date 2022/8/1 20:13
 * @Created by wangchangjiu
 */

@Data
public class VipRenewalOrder {

    /**
     *  订单编号
     */
    private String orderSn;

    /**
     *  上次订单号
     */
    private String latestOrderSn;

    /**
     *  扣款期数
     */
    private Integer subscriptionIndex;


    /**
     *  签约号
     */
    private String signNo;

    /**
     *  用户ID
     */
    private Long userId;

    /**
     *  商品 ID
     */
    private Long goodsId;

    /**
     *  固化商品
     */
    private String goodsInfo;

    /**
     *  售价 单位到分
     */
    private Integer price;


    /**
     *  签约渠道
     */
    private String signChannel;


    /**
     *  签约状态 订阅状态，0-等待订阅 1-订阅成功 2-订阅失败 3-订阅取消 4-订阅失效 5-订阅计划暂停
     */
    private Integer signStatus;


    private String statusReason;

    /**
     *  本期扣款状态
     *  0-待扣款 1-扣款成功 2-扣款失败 3-扣款取消 4-已忽略
     */
    private Integer deductStatus;

    /**
     *  第三方续订内容
     */
    private String renewalContent;

    /**
     *  开始订阅时间 以 三方为准
     */
    private Date startOrderTime;


    /**
     *  上一次订单自动续约时间
     */
    private Date latestOrderTime;


    /**
     *  理论上下次扣款时间
     */
    private Date nextOrderTime;

/*    *//**
     *  google 本次新 token
     *//*
    private String purchaseToken;

    *//**
     *  解决google 重新订阅问题 上一次的 token
     *//*
    private String linkedPurchaseToken;*/

   /* public UserSubscribeGoodsVO buildUserSubscribeGoodsVO(String lang){
        UserSubscribeGoodsVO goodsVO = new UserSubscribeGoodsVO();
        MarketVipGoodsVo marketVipGoodsVo = JSON.parseObject(this.getGoodsInfo(), MarketVipGoodsVo.class);
        String goodsNameDefaultEn = marketVipGoodsVo.getGoodsNameDefaultEn(lang);
        goodsVO.setSignNo(this.getSignNo());
        goodsVO.setGoodsName(goodsNameDefaultEn);
        goodsVO.setExpiredTime(this.getNextOrderTime().getTime());
        goodsVO.setSignChannel(this.getSignChannel());
        return goodsVO;
    }*/

}
