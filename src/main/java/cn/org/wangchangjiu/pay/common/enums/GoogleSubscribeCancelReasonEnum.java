package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname GoogleSubscribeCancelReasonEnum
 * @Description
 * 0. User canceled the subscription 用户取消了订阅
 * 1. Subscription was canceled by the system, for example because of a billing problem 例如，由于计费问题，系统取消了订阅
 * 2. Subscription was replaced with a new subscription 订阅已替换为新的订阅
 * 3. Subscription was canceled by the developer 开发商已取消订阅
 * @Date 2023/1/5 17:12
 * @Created by wangchangjiu
 */
public enum GoogleSubscribeCancelReasonEnum {

    // 0。用户取消了订阅1。例如，由于计费问题2，系统取消了订阅。订阅已替换为新的订阅3。开发商已取消订阅
    USER_CANCELED(0, "用户取消了订阅"),
    SETTLEMENT_ISSUES(1, "系统取消了订阅，例如：由于结算问题取消订阅"),
    REPLACE_SUBSCRIBE(2, "订阅被新的订阅被新的订阅替换"),
    DEVELOPER_CANCELED(3, "开发者已取消订阅"),
    INVALID(4, "订阅已经失效"),
    USER_SUSPEND(8, "用户暂停了订阅计划")
    ;

    private final Integer code;
    private final String cancelReason;

    GoogleSubscribeCancelReasonEnum(Integer code, String cancelReason) {
        this.code = code;
        this.cancelReason = cancelReason;
    }

    public static String getCancelReason(Integer code){
        for (GoogleSubscribeCancelReasonEnum cancelReasonEnum : GoogleSubscribeCancelReasonEnum.values()) {
            if(code.equals(cancelReasonEnum.getCode())){
                return cancelReasonEnum.getCancelReason();
            }
        }
        return "";
    }

    public Integer getCode() {
        return code;
    }

    public String getCancelReason() {
        return cancelReason;
    }
}
