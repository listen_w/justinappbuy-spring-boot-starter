package cn.org.wangchangjiu.pay.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @Classname PayMethodEnum
 * @Description
 * @Date 2022/8/2 18:13
 * @Created by wangchangjiu
 */
public enum PayMethodEnum {

    // google
    PAYER_MAX_H5("payer_max_h5"),
    GOOGLE_PAY_PRIMORDIAL("google_pay_pri"),
    PAY_PAL_H5("paypal_pay_h5"),
    APPLE_PAY("ApplePay"),
    ;


    private final String code;

    PayMethodEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static boolean checkSupportPayMethod(String code) {
        if (StringUtils.isEmpty(code)) {
            return false;
        }
        for (PayMethodEnum payMethodEnum : PayMethodEnum.values()) {
            if (code.equalsIgnoreCase(payMethodEnum.getCode())) {
                return true;
            }
        }
        return false;
    }
}
