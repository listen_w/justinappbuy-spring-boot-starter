package cn.org.wangchangjiu.pay.common;

/**
 * @Classname InAppBuyChannel
 * @Description
 * @Date 2023/6/14 16:44
 * @Created by wangchangjiu
 */
public enum InAppBuyChannel {

    GOOGLE,

    APPLE,

    PAYER_MAX

}
