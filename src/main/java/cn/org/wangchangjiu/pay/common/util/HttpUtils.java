package cn.org.wangchangjiu.pay.common.util;

import java.util.Map;
import com.xkcoding.http.*;
import com.xkcoding.http.config.HttpConfig;
import com.xkcoding.http.support.HttpHeader;

/**
 * @Classname HttpUtils
 * @Description
 * @Date 2023/6/19 21:13
 * @Created by wangchangjiu
 */
public class HttpUtils {

    public HttpUtils(HttpConfig config) {
        HttpUtil.setConfig(config);
    }

    public HttpUtils() {
    }

    public String get(String url) {
        return HttpUtil.get(url);
    }

    public String get(String url, Map<String, String> params, HttpHeader header, boolean encode) {
        return HttpUtil.get(url, params, header, encode);
    }

    public String post(String url) {
        return HttpUtil.post(url);
    }

    public String post(String url, String data) {
        return HttpUtil.post(url, data);
    }

    public String post(String url, String data, HttpHeader header) {
        return HttpUtil.post(url, data, header);
    }

    public String post(String url, Map<String, String> params, boolean encode) {
        return HttpUtil.post(url, params, encode);
    }

    public String post(String url, Map<String, String> params, HttpHeader header, boolean encode) {
        return HttpUtil.post(url, params, header, encode);
    }

}
