package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname PayerMaxEventReasonEnum
 * @Description
 * 1、订阅计划请求通知
 * 2、系统自动扣款通知
 * 4、商户主动扣款通知
 * @Date 2023/1/5 17:12
 * @Created by wangchangjiu
 */
public enum PayerMaxDeductionEventReasonEnum {

    PLAN_REQUEST(1, "订阅计划请求通知"),
    SYSTEM_AUTOMATIC_DEDUCTION(2, "系统自动扣款通知"),
    ACTIVE_DEDUCTION(4, "商户主动扣款通知")
    ;

    private final Integer code;
    private final String eventReason;

    PayerMaxDeductionEventReasonEnum(Integer code, String eventReason) {
        this.code = code;
        this.eventReason = eventReason;
    }

    public static String getEventReason(Integer code){
        for (PayerMaxDeductionEventReasonEnum eventReasonEnum : PayerMaxDeductionEventReasonEnum.values()) {
            if(code.equals(eventReasonEnum.getCode())){
                return eventReasonEnum.getEventReason();
            }
        }
        return "";
    }

    public Integer getCode() {
        return code;
    }

    public String getEventReason() {
        return eventReason;
    }
}
