package cn.org.wangchangjiu.pay.common;

import lombok.Data;

/**
 * @Classname VerifyException
 * @Description
 * @Date 2023/6/14 17:16
 * @Created by wangchangjiu
 */
@Data
public class VerifyException extends RuntimeException {

    private String code;

    private String message;

    public VerifyException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }


}
