package cn.org.wangchangjiu.pay.common.enums;

import groot.foundation.util.StringUtils;
import groot.order.common.Constant;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.Stream;

/**
 * @Classname PayMethodEnum
 * @Description
 * @Date 2022/8/2 18:13
 * @Created by wangchangjiu
 */
@Slf4j
public enum VipPayMethodEnum {

    PAYER_DANA_H5("payer_dana_h5", "dana"),
    PAYER_TRUEMONEY_H5("payer_truemoney_h5", "truemoney"),
    PAYER_GCASH_H5("payer_gcash_h5", "gcash"),
    PAYER_GTOUCHNGO_H5("payer_gtouchngo_h5", "gtouchngo"),
    GOOGLE_PAY_PRIMORDIAL("google_pay_pri", "google"),
    PAY_PAL_H5("paypal_pay_h5", "paypal"),
    APPLE_PAY("ApplePay", "ApplePay"),
    ;


    private final String code;
    private final String channel;

    VipPayMethodEnum(String code, String channel) {
        this.code = code;
        this.channel = channel;
    }

    /**
     *  获取 PayerMax 支付编码
     * @param countryIsoCode
     * @param payMethod
     * @return
     */
    public static String getPayerMaxPayMethodCode(String countryIsoCode, String payMethod) {
        VipPayMethodEnum vipPayMethodEnum = Stream.of(VipPayMethodEnum.values())
                .filter(payMethodEnum -> payMethod.equalsIgnoreCase(payMethodEnum.getCode())).findFirst().get();
        String key = countryIsoCode.concat("_").concat(vipPayMethodEnum.getChannel()).toLowerCase();
        String payMethodCode = Constant.PayerMax.PAY_METHOD_CODE_MAP.get(key);
        log.info("获取支付编码Key:{}, payMethodCode:{}", key, payMethodCode);
        return payMethodCode;
    }

    public String getCode() {
        return code;
    }

    public String getChannel() {
        return channel;
    }

    public static boolean checkSupportPayMethod(String code){
        if(StringUtils.isEmpty(code)){
            return false;
        }
        for (VipPayMethodEnum payMethodEnum : VipPayMethodEnum.values()) {
            if(code.equalsIgnoreCase(payMethodEnum.getCode())){
                return true;
            }
        }
        return false;
    }
}
