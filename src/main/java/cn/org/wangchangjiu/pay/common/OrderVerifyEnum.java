package cn.org.wangchangjiu.pay.common;

/**
 * @Classname GooglePayVerifyEnum
 * @Description
 * @Date 2022/8/2 17:37
 * @Created by wangchangjiu
 */
public enum OrderVerifyEnum {

    RSA_VERIFY_FAIL("E0002", "RSA 校验失败"),
    ORDER_HANDLED("E0005", "订单已经处理"),
    ORDER_UN_PAID("E0010", "订单未支付"),
    ORDER_NOT_FOUND("E0020", "订单不存在"),
    PAY_PAL_CREATE_ORDER_ERROR("E1000", "第三方服务PayPal创建订单失败"),
    PAY_PAL_CAPTURE_ORDER_ERROR("E1000", "第三方服务PayPal核销订单失败"),
    GOODS_PRICE_CHANGE("E2000", "创建订单失败,商品价格变动"),
    GOODS_NOT_FOUND("E2000", "创建订单失败,商品不存在"),
    PAYER_MAX_CREATE_ORDER_ERROR("E3000", "第三方服务 PayerMax 创建订单失败"),
    ORDER_PAY_DISABLE_APPEAL("E4001","订单支付禁用，可申诉"),
    ORDER_PAY_DISABLE_NON_APPEAL("E4002","订单支付禁用，不可申诉"),

    WATCH_ORDER_EXISTS_ORDER_ING("E5000", "有进行中的订单"),
    WATCH_ORDER_CREATE_FAIL("E6000", "陪看订单创建失败提醒");

    private final String code;
    private final String desc;

    OrderVerifyEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static OrderVerifyEnum getOrderPayDisable(boolean isAppeal){
        return isAppeal ? OrderVerifyEnum.ORDER_PAY_DISABLE_NON_APPEAL : OrderVerifyEnum.ORDER_PAY_DISABLE_APPEAL;
    }

}
