package cn.org.wangchangjiu.pay.common;

import cn.org.wangchangjiu.pay.common.dto.VipOrder;
import cn.org.wangchangjiu.pay.common.dto.VipRenewalOrder;
import cn.org.wangchangjiu.pay.google.request.GooglePayVerifyRequest;

import java.util.Map;

/**
 * @Classname InAppBuyDataRepository
 * @Description 应用内购买订单数据仓库 由用户自己实现
 * @Date 2023/6/14 17:24
 * @Created by wangchangjiu
 */
public interface InAppBuyDataRepository {

    /**
     *  获取商品
     * @param params
     * @return
     */
    MarketVipGoodsVo findMarketGoods(Map<String, Object> params);

    /**
     *  根据三方订单号获取订单
     * @param orderId
     * @return
     */
    VipOrder findByThirdOrderSn(String orderId);

    /**
     *  获取包与公钥
     * @return
     */
    default Map<String, String> getPackageAndPublicKey() {
        throw new RuntimeException("no su");
    }

    /**
     *
     * @param linkedPurchaseToken
     * @param purchaseToken
     * @return
     */
    VipRenewalOrder findVipRenewalOrderByLinkedPurchaseTokenAndPurchaseToken(String linkedPurchaseToken, String purchaseToken);

    /**
     *
     * @param linkedPurchaseToken
     * @return
     */
    VipRenewalOrder findVipRenewalOrderByPurchaseToken(String linkedPurchaseToken);

    void saveVipRenewalOrder(VipRenewalOrder vipRenewalOrder);

    VipOrder createOrderByGooglePay(GooglePayVerifyRequest inAppVerifyRequest, MarketVipGoodsVo marketVipGoodsVo);

    void updateOrderFail(String id, String originInfo);

    void updateOrder(VipOrder order);

    void updateOrderPaid(String id, String originInfo);

    void updateOrderDeliverSuccess(VipOrder order);
}
