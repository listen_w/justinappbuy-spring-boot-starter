package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname OrderStatusEnum
 * @Description
 * @Date 2022/8/2 18:15
 * @Created by wangchangjiu
 */
public enum OrderStatusEnum {

    // 0：初始化 1：已完成 2 已经取消 3 已退款 5 已支付 9 失败 7 退款中 11 退款失败
    INIT(0),
    PAY(5),
    COMPLETE(1),
    CANCEL(2),
    REFUND(3),
    FAILED(9),
    REFUND_ING(7),
    REFUND_FAILED(11)
    ;

    private final Integer code;

    OrderStatusEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public static String findNameByCode(Integer code){
        String statusName = "";
        if(OrderStatusEnum.INIT.getCode().equals(code)){
            statusName = "初始化";
        } else if(OrderStatusEnum.PAY.getCode().equals(code)){
            statusName = "已支付";
        } else if(OrderStatusEnum.COMPLETE.getCode().equals(code)){
            statusName = "已完成";
        } else if(OrderStatusEnum.CANCEL.getCode().equals(code)){
            statusName = "已经取消";
        } else if(OrderStatusEnum.REFUND.getCode().equals(code)){
            statusName = "已退款";
        } else if(OrderStatusEnum.FAILED.getCode().equals(code)){
            statusName = "失败";
        } else if(OrderStatusEnum.REFUND_ING.getCode().equals(code)){
            statusName = "退款中";
        } else if(OrderStatusEnum.REFUND_FAILED.getCode().equals(code)){
            statusName = "退款失败";
        }
        return statusName;
    }

}
