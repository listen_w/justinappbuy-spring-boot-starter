package cn.org.wangchangjiu.pay.common.enums;

import lombok.Getter;

/**
 * @Classname AppleResponseStatusEnum
 * @Description 苹果响应状态码
 * @Date 2022/12/9 15:45
 * @Created by wangchangjiu
 */
@Getter
public enum AppleResponseStatusEnum {
    /**
     * 未等待苹果响应直接返回
     */
    NO_RESPONSE(-1, "未等待苹果响应直接返回"),
    /**
     * 成功
     */
    SUCCESS(0, "success"),
    STATUS_21000(21000, "对App Store的请求不是使用HTTP POST请求方法发出的。"),
    STATUS_21001(21001, "这个状态码不再被App Store发送。"),
    STATUS_21002(21002, "接收-数据属性中的数据格式错误或服务遇到临时问题。再试一次。"),
    STATUS_21003(21003, "这张收据无法认证"),
    STATUS_21004(21004, "您提供的共享密钥与您帐户文件中的共享密钥不匹配。"),
    STATUS_21005(21005, "收据服务器暂时无法提供收据。再试一次。"),
    STATUS_21006(21006, "此收据有效，但订阅已过期。当此状态码返回到服务器时，接收数据也将被解码并作为响应的一部分返回。只有iOS 6风格的自动更新订阅交易收据才会返回。"),
    STATUS_21007(21007, "此收据来自测试环境，但它被发送到生产环境进行验证。"),
    STATUS_21008(21008, "此收据来自生产环境，但它被发送到测试环境进行验证。"),
    STATUS_21009(21009, "内部数据访问错误。稍后再试。"),
    STATUS_21010(21010, "用户帐户无法找到或已被删除。");

    private Integer status;

    private String msg;

    AppleResponseStatusEnum(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public static String getMsgByCode(Integer code) {
        for (AppleResponseStatusEnum enumItem : AppleResponseStatusEnum.values()) {
            if (enumItem.getStatus().equals(code)) {
                return enumItem.getMsg();
            }
        }
        return "";
    }
}
