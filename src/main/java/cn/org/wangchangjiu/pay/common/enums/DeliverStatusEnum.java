package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname DeliverStatusEnum
 * @Description
 * @Date 2022/8/3 17:25
 * @Created by wangchangjiu
 */
public enum DeliverStatusEnum {

    // 发货状态：0：未发货 1：发货成功
    UNDELIVERED(0),
    SUCCESS(1)
    ;

    private final Integer code;

    DeliverStatusEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
