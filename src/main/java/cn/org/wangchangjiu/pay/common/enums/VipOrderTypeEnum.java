package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname OrderStatusEnum
 * @Description
 * @Date 2022/8/2 18:15
 * @Created by wangchangjiu
 */
public enum VipOrderTypeEnum {

    //  0 一次性购买 1 周期扣款订单
    PURCHASE(0),
    SUBSCRIPTION(1),
    ;

    private final Integer code;

    VipOrderTypeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
