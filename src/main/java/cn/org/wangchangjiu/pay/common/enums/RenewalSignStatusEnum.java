package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname RenewalSignStatusEnum
 * @Description
 * @Date 2022/11/4 17:38
 * @Created by wangchangjiu
 */
public enum RenewalSignStatusEnum {

    // 订阅状态，0-等待订阅 1-订阅成功 2-订阅失败 3-订阅取消 4-订阅失效
    WAITING(0),
    SUCCESS(1),
    FAIL(2),
    CANCEL(3),
    INVALID(4),
    SUSPEND(8);

    private final Integer code;

    RenewalSignStatusEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
