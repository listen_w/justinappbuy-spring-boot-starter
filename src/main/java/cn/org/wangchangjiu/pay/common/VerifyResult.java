package cn.org.wangchangjiu.pay.common;

/**
 * @Classname VerifyResult
 * @Description
 * @Date 2023/6/14 11:30
 * @Created by wangchangjiu
 */
public interface VerifyResult<T> {

    T getData();

}
