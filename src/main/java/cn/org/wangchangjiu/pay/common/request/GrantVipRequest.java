package cn.org.wangchangjiu.pay.common.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Classname GrantVipRequest
 * @Description
 * @Date 2023/6/19 20:29
 * @Created by wangchangjiu
 */
@Data
public class GrantVipRequest implements Serializable {

    /**
     *  VIP类型ID
     */
    private Long vipLevelId;

    /**
     *  发放的用户
     */
    private List<Long> userIds;

    /**
     *  发放设备
     */
    private String deviceId;

    /**
     *  发放数量
     */
    private Integer num;

    /**
     *  单位 VipDeductIntervalEnum，code，天、周、月
     */
    private String unit;

    /**
     *  产品产出方式ProduceModeEnum，vip发放来源
     */
    private String type;

    /**
     *  关联ID，发放类型是VIP充值，则关联的就是订单号
     */
    private String associationId;

}
