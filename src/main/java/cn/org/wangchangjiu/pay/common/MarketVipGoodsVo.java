package cn.org.wangchangjiu.pay.common;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @Classname MarketGoodsVo
 * @Description 上架商品VO
 * @Date 2022/11/2 11:48
 * @Created by wangchangjiu
 */

@Data
public class MarketVipGoodsVo implements Serializable {

    /**
     *  商品ID
     */
    private Long goodsId;

    /**
     *  vip 会员等级
     */
    private Long vipLevelId;

    /**
     *  国家
     */
    private String countryIsoCode;

    /**
     *  原价
     */
    private Integer originalPrice;


    /**
     *  首购价
     */
    private Integer firstBuyPrice;

    /**
     *  续订价
     */
    private Integer renewalPrice;

    /**
     *  发放数量
     */
    private Integer num;


    /**
     *  是否是订阅产品
     */
    private boolean subscribed;

    /**
     *  是否有优惠
     */
    private Boolean preferential = false;

    /**
     *  商品名称
     */
    private List<LanguageVo> languageGoodsName;

    /**
     *  扣款周期
     */
    private VipDeductIntervalEnum intervalEnum;


    public String getGoodsNameDefaultEn(String language) {
        return StringUtils.isNotEmpty(this.getOrderName(language)) ? this.getOrderName(language) : this.getOrderName("en");
    }

    public String getOrderName(String lang){
        Optional<LanguageVo> first = this.getLanguageGoodsName().stream().filter(item -> item.getLanguage().equals(lang)).findFirst();
        if(first.isPresent()){
            return first.get().getName();
        }
        return null;
    }

    public Integer getNum() {
        VipDeductIntervalEnum deductInterval = this.getIntervalEnum();
        if(VipDeductIntervalEnum.D == deductInterval){
            return num;
        } else if(VipDeductIntervalEnum.W == deductInterval){
            return 7;
        } else if(VipDeductIntervalEnum.M == deductInterval){
            return 31;
        } else if(VipDeductIntervalEnum.Q == deductInterval){
            return 92;
        } else if(VipDeductIntervalEnum.Y == deductInterval){
            return 366;
        }
        return num;
    }

    // 发放天数

    public boolean checkGoods(){
        if(this == null){
            return false;
        }
        if(this.isSubscribed()){
            // 订阅商品 原价和续订价 都不为空
            return this.getOriginalPrice() != null && this.getRenewalPrice() != null;
        }
        // 非续订商品 原价不为空
        return this.getOriginalPrice() != null;
    }
}
