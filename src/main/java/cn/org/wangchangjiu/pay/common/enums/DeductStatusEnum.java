package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname PayerMaxDeductStatusEnum
 * @Description payerMax 自动扣款状态
 * @Date 2022/11/10 14:11
 * @Created by wangchangjiu
 */
public enum DeductStatusEnum {

    // 0-待扣款 1-扣款成功 2-扣款失败 3-扣款取消 4-已忽略
    WAIT_DEDUCTED(0),
    DEDUCTED_SUCCESS(1),
    DEDUCTED_FAIL(2),
    CANCEL_DEDUCTED(3),
    IGNORE_DEDUCTED(4),

    ;

    private final Integer code;

    DeductStatusEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public static boolean hasDeductedSuccess(Integer code){
      return findByCode(code) == DEDUCTED_SUCCESS;
    }

    public static DeductStatusEnum findByCode(Integer code){
        for(DeductStatusEnum deductStatusEnum: DeductStatusEnum.values()){
            if(deductStatusEnum.getCode().equals(code)){
               return deductStatusEnum;
            }
        }
        return null;
    }

}
