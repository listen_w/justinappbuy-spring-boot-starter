package cn.org.wangchangjiu.pay.common.dto;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * @Classname VipOrder
 * @Description 会员订单
 * @Date 2022/8/1 20:13
 * @Created by wangchangjiu
 */

@Data
public class VipOrder {

    private String id;

    /**
     *  订单编号
     */
    private String orderSn;

    /**
     *  第三方订单编号
     */
    private String thirdOrderSn;


    /**
     *  用户ID
     */
    private String userId;


    /**
     *  订单类型 0 一次性购买 1 周期扣款订单
     */
    private int type;

    /**
     *  商品 ID
     */
    private String goodsId;

    /**
     *  固化商品信息
     */
    private String goodsInfo;

    /**
     *  售价 单位到分
     */
    private Integer price;


    /**
     *  付款方式  Google pay
     */
    private String paymentMethod;

    /**
     *  订单状态：0：初始化 1：已完成 2 已经取消 3 已退款 5 已经支付 9 订单失败 7 退款中
     */
    private Integer status;

    /**
     *  发货状态：0：发货失败 1：发货成功
     */
    private Integer deliverStatus;


    /**
     *  订单付款时间
     */
    private Date payTime;

    /**
     *  订单完成时间
     */
    private Date completeTime;

    /**
     *  订单完成时间
     */
    private Date refundTime;


    /**
     *  分销渠道
     */
    private String distributionChannel;

    /**
     *  包名
     */
    private String packageName;

    /**
     *  第三方支付信息
     */
    private String originInfo;



    /**
     *  备注
     */
    private String remark;


    /**
     *  退款原因
     */
    private String refundReason;

    /**
     *  下单用户设备
     */
    private String deviceId;

    /**
     *  下单用户 IP
     */
    private String ip;

    /**
     *  国家
     */
    private String country;

    /**
     *  是否支持退款
     */
    private Boolean supportRefund = false;

    /**
     *  检查订单是否已经处理
     * @return
     */
    public boolean checkHandleStatus() {
        return this.status != 0;
    }

    /**
     *  是否需要更新订单数据
     * @return
     */
    public boolean needUpdateData() {
        return this.getIp() == null || this.getPrice() == null || this.getDeviceId() == null || StringUtils.isEmpty(this.getDistributionChannel());
    }

    /**
     *  能否申请退款
     * @return
     */
  /*  public boolean canApplyRefund() {
        return (OrderStatusEnum.COMPLETE.getCode().equals(this.getStatus())
                || OrderStatusEnum.PAY.getCode().equals(this.getStatus()))
                && this.getSupportRefund();
    }*/

    /**
     *  是否是订阅单子
     * @return
     */
   /* public boolean subscriptionOrder(){
       return this.getType() == VipOrderTypeEnum.SUBSCRIPTION.getCode();
    }*/

    /**
     *  能否完成退款
     * @return
     */
    public boolean canRefund() {
        return this.getStatus() == 7;
    }

    public boolean statusNoInit() {
        return this.getStatus() != 0;
    }

    /**
     *  获取退款订单号
     * @return
     */
    public String getRefundOrderSn(){
        return this.getOrderSn() + "_r";
    }

    /**
     *  订单是否处于 已经支付却未发货状态
     * @return
     */
    /*public boolean orderPaidNotDeliver(){
        return OrderStatusEnum.PAY.getCode().equals(this.getStatus())
                && DeliverStatusEnum.UNDELIVERED.getCode().equals(this.getDeliverStatus());
    }
*/
    /**
     *  拷贝 PayerMax订单
     * @param callback
     * @param status
     * @return
     */
    /*public VipOrder copyPayerMaxOrder(PayerMaxSubscribeCallback callback, H5MarketVipGoodsVo marketVipGoodsVo, Integer status, String orderSn){
        VipOrder vipOrder = new VipOrder();
        vipOrder.setPrice(marketVipGoodsVo.getRenewalPrice());
        vipOrder.setStatus(status);
        vipOrder.setPayTime(new Date());
        String paymentMethod = Constant.PayerMax.PAY_CODE_METHOD_MAP.get(callback.getPaymentMethod());
        vipOrder.setPaymentMethod(StringUtils.defaultString(paymentMethod, this.getPaymentMethod()));
        vipOrder.setPackageName(this.getPackageName());
        vipOrder.setType(VipOrderTypeEnum.SUBSCRIPTION.getCode());
        vipOrder.setGoodsId(this.getGoodsId());
        vipOrder.setGoodsInfo(this.getGoodsInfo());
        vipOrder.setUserId(this.getUserId());
        vipOrder.setIp(this.getIp());
        vipOrder.setDistributionChannel(this.getDistributionChannel());
        vipOrder.setDeviceId(this.getDeviceId());
        vipOrder.setCountry(callback.getCountryCode());
        vipOrder.setOrderSn(orderSn);
        vipOrder.setThirdOrderSn(callback.getDeductNo());
        return vipOrder;
    }

    public VipOrderVo build(Map<Long, SimpleGoodsVo> goodsMap){
        VipOrderVo orderVo = new VipOrderVo();
        orderVo.setId(this.getId());
        orderVo.setDeviceId(this.getDeviceId());
        orderVo.setThirdOrderSn(this.getThirdOrderSn());
        orderVo.setOrderSn(this.getOrderSn());
        orderVo.setCreateDate(this.getCreateTime());
        orderVo.setDeliverStatus(this.getDeliverStatus());
        orderVo.setUserId(this.getUserId());
        orderVo.setPaymentMethod(this.getPaymentMethod());
        orderVo.setStatus(this.getStatus());
        orderVo.setPrice(this.getPrice());
        orderVo.setSupportRefund(this.getSupportRefund());
        SimpleGoodsVo goodsVo = goodsMap.get(this.getGoodsId());
        if(goodsVo != null){
            orderVo.setGoodsName(goodsVo.getName());
        }
        orderVo.setCountry(this.getCountry());
        orderVo.setRefundReason(this.getRefundReason());
        String distributionChannel = this.getDistributionChannel();
        orderVo.setDistributionChannel(distributionChannel);
        orderVo.setAbnormal(VipOrderAbnormalReasonEnum.formatNamesByCode(this.abnormal));
        orderVo.setAbnormalReason(this.getAbnormalReason());
        orderVo.setAbnormalStatus(this.getAbnormalStatus());
        return orderVo;
    }

    public VipOrderExportVo buildExport(Map<Long, SimpleGoodsVo> goodsMap){
        VipOrderExportVo orderVo = new VipOrderExportVo();
        orderVo.setId(this.getId());
        orderVo.setDeviceId(this.getDeviceId());
        orderVo.setThirdOrderSn(this.getThirdOrderSn());
        orderVo.setOrderSn(this.getOrderSn());
        orderVo.setCreateDate(this.getCreateTime());
        orderVo.setDeliverStatus(this.getDeliverStatus());
        orderVo.setUserId(this.getUserId());
        orderVo.setPaymentMethod(this.getPaymentMethod());
        orderVo.setStatus(this.getStatus());
        orderVo.setPrice(this.getPrice());
        SimpleGoodsVo goodsVo = goodsMap.get(this.getGoodsId());
        if(goodsVo != null){
            orderVo.setGoodsName(goodsVo.getName());
        }
        orderVo.setCountry(this.getCountry());
        String distributionChannel = this.getDistributionChannel();
        orderVo.setDistributionChannel(distributionChannel);
        return orderVo;
    }


    public VipOrderAbnormalExportVo buildAbnormalExport(Map<Long, SimpleGoodsVo> goodsVoMap) {
        VipOrderAbnormalExportVo orderVo = new VipOrderAbnormalExportVo();
        orderVo.setId(this.getId());
        orderVo.setDeviceId(this.getDeviceId());
        orderVo.setThirdOrderSn(this.getThirdOrderSn());
        orderVo.setOrderSn(this.getOrderSn());
        orderVo.setCreateDate(this.getCreateTime());
        orderVo.setDeliverStatus(this.getDeliverStatus());
        orderVo.setUserId(this.getUserId());
        orderVo.setPaymentMethod(this.getPaymentMethod());
        orderVo.setStatus(this.getStatus());
        orderVo.setPrice(this.getPrice());
        SimpleGoodsVo goodsVo = goodsVoMap.get(this.getGoodsId());
        if(goodsVo != null){
            orderVo.setGoodsName(goodsVo.getName());
        }
        orderVo.setCountry(this.getCountry());
        String distributionChannel = this.getDistributionChannel();
        orderVo.setDistributionChannel(distributionChannel);
        orderVo.setAbnormal(this.getAbnormal());
        orderVo.setAbnormalStatus(this.getAbnormalStatus());
        return orderVo;
    }*/
}
