package cn.org.wangchangjiu.pay.common;

import lombok.Data;

import java.util.Map;

/**
 * @Classname InAppVerifyParam
 * @Description
 * @Date 2023/6/14 11:07
 * @Created by wangchangjiu
 */
@Data
public class InAppVerifyParam<T> {

    private T inAppVerifyRequest;

    private Map<String, Object> params;
}
