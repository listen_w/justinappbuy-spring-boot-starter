package cn.org.wangchangjiu.pay.common.util;

import cn.org.wangchangjiu.pay.google.service.GoogleInAppBuyService;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * @Classname ConstantUtil
 * @Description
 * @Date 2023/6/14 17:15
 * @Created by wangchangjiu
 */
public class ConstantUtil {

    private static final Logger log = LoggerFactory.getLogger(GoogleInAppBuyService.class);

    public static class GooglePay {

        public static boolean doCheck(String content, String sign, String publicKey) {
            try {
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                byte[] encodedKey = Base64.getDecoder().decode(publicKey);
                PublicKey pubKey = keyFactory
                        .generatePublic(new X509EncodedKeySpec(encodedKey));
                Signature signature = Signature.getInstance("SHA1WithRSA");
                signature.initVerify(pubKey);
                signature.update(content.getBytes("utf-8"));
                return signature.verify(Base64.getDecoder().decode(sign));
            } catch (Exception e) {
                log.error("Google pay RSA 校验异常：{}", e);
            }
            return false;
        }


        public static boolean checkSubscriptionPurchase(SubscriptionPurchase subscriptionPurchase) {
            return false;
        }
    }



}
