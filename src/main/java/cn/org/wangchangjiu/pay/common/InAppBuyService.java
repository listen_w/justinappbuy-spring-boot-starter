package cn.org.wangchangjiu.pay.common;

import cn.org.wangchangjiu.pay.common.dto.VipOrder;

import java.util.Map;

/**
 * @Classname InAppBuyService
 * @Description 应用内购买服务
 * @Date 2023/6/13 21:15
 * @Created by wangchangjiu
 */
public interface InAppBuyService<T,R> {

    VerifyResult verify(T inAppVerifyRequest, Map<String, Object> params);

    void subscribeNotify(R r);

    void applyRefund(VipOrder vipOrder);

    default void capture() {

    }

}
