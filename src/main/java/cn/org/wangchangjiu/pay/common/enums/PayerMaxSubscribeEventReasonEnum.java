package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname PayerMaxEventReasonEnum
 * @Description
 * 1、授权失败，订阅取消
 * 2、系统自动扣款任务
 * 3、订阅支付完成
 * 4、商户主动扣款
 * 5、订阅支付取消
 * 6、订阅计划终止
 *
 * @Date 2023/1/5 17:12
 * @Created by wangchangjiu
 */
public enum PayerMaxSubscribeEventReasonEnum {

    AUTHORITY_FAIL_CANCELED(1, "授权失败，订阅取消"),
    SUBSCRIPTION_PAY_COMPLETED(3, "订阅支付完成"),
    SUBSCRIPTION_PAY_CANCELED(5, "订阅支付取消"),
    SUBSCRIPTION_PLAN_TERMINATION(6, "订阅计划终止")
    ;

    private final Integer code;
    private final String eventReason;

    PayerMaxSubscribeEventReasonEnum(Integer code, String eventReason) {
        this.code = code;
        this.eventReason = eventReason;
    }

    public static String getEventReason(Integer code){
        for (PayerMaxSubscribeEventReasonEnum eventReasonEnum : PayerMaxSubscribeEventReasonEnum.values()) {
            if(code.equals(eventReasonEnum.getCode())){
                return eventReasonEnum.getEventReason();
            }
        }
        return "";
    }

    public Integer getCode() {
        return code;
    }

    public String getEventReason() {
        return eventReason;
    }
}
