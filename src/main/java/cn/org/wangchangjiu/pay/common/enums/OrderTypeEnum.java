package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname OrderTypeEnum
 * @Description
 * @Date 2022/8/2 18:18
 * @Created by wangchangjiu
 */
public enum OrderTypeEnum {

    // 0 充值订单 1 会员订单
    RECHARGE(0),
    VIP(1)
    ;


    private final Integer code;

    OrderTypeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
