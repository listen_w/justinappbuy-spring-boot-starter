package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname RetryVerifyReceiptStatus
 * @Description
 * @Date 2022/12/14 13:51
 * @Created by wangchangjiu
 */
public enum RetryVerifyReceiptStatus {

    // 0 重试成功 1 重试中 2 达到次数重试失败
    SUCCESS(0),
    RETRY_ING(1),
    FAIL(2);

    private final Integer code;

    RetryVerifyReceiptStatus(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
