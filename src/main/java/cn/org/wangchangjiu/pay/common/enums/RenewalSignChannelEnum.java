package cn.org.wangchangjiu.pay.common.enums;

import java.util.Arrays;
import java.util.List;

/**
 * @Classname RenewalSignChannelEnum
 * @Description
 * @Date 2022/11/8 19:57
 * @Created by wangchangjiu
 */
public enum RenewalSignChannelEnum {

    GOOGLE("google"),
    PAYER_MAX("payer_max"),
    PAYPAL("paypal");

    private final String code;

    RenewalSignChannelEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static List<String> getH5Channels(){
        return Arrays.asList(PAYER_MAX.getCode(), PAYPAL.getCode());
    }

}
