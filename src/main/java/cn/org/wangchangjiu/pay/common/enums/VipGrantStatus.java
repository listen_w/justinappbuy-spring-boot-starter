package cn.org.wangchangjiu.pay.common.enums;

/**
 * @Classname VipGrantStatus
 * @Description
 * @Date 2022/12/13 17:20
 * @Created by wangchangjiu
 */
public enum VipGrantStatus {

    // 发货状态：0：未发货 1：发货成功
    UNDELIVERED(0),
    SUCCESS(1),
    SANDBOX_NOT_GRANT(2)
    ;

    private final Integer code;

    VipGrantStatus(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
