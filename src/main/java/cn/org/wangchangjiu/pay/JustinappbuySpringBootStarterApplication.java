package cn.org.wangchangjiu.pay;

import cn.org.wangchangjiu.pay.common.InAppBuyService;
import cn.org.wangchangjiu.pay.common.VerifyResult;
import cn.org.wangchangjiu.pay.google.request.GooglePayVerifyRequest;
import cn.org.wangchangjiu.pay.google.service.GoogleInAppBuyService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;

@SpringBootApplication
public class JustinappbuySpringBootStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(JustinappbuySpringBootStarterApplication.class, args);

		/*GoogleInAppBuyService inAppBuyService = new GoogleInAppBuyService();
		VerifyResult verify = inAppBuyService.verify(new GooglePayVerifyRequest(), new HashMap<>());
		Object data = verify.getData();*/

	}

}
