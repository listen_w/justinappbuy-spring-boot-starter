package cn.org.wangchangjiu.pay.apple.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 付款回执查询结果
 * <a href="https://developer.apple.com/documentation/appstorereceipts/responsebody">苹果官方说明文档</a>
 *
 * @author
 */
@Data
public class AppleVerifyReceiptVo {

    /**
     * 运行环境:Production - 生产环境;Sandbox - 沙盒环境
     */
    @JsonProperty(value = "environment")
    private String environment;

    /**
     * 是否可以重试
     * 1 - 临时故障，可以重试
     * 0 - 不能重试
     * 仅在status为21100至21199时出现
     */
    @JsonProperty(value = "is-retryable")
    private Boolean retryable;
    /**
     * 最近的回执信息
     * base64编码的app回执信息，仅在回执包含自动续订信息时返回
     */
    @JsonProperty(value = "latest_receipt")
    private String latestReceipt;
    /**
     * 一个包含所有app内购交易的数组
     * 不包含在app内已标识为已完成的交易。仅在回执包含自动续订信息时返回
     */
    @JsonProperty(value = "latest_receipt_info")
    private List<LatestReceiptInfoVo> latestReceiptInfo;
    /**
     * 一个包含所有未确认的自动订阅交易的数组
     * 仅在回执包含自动续订信息时返回
     */
    @JsonProperty(value = "pending_renewal_info")
    private List<PendingRenewalInfoVo> pendingRenewalInfo;
    /**
     * 一个包含所有验证订单信息的数组
     */
    private ReceiptVo receipt;
    /**
     * 状态码:
     * 0 成功
     * 21000 对App Store的请求不是使用HTTP POST请求方法发出的。
     * 21001 该状态码不再由App Store发送。
     * 21002 接收-数据属性中的数据格式错误或服务遇到临时问题。再试一次。
     * 21003
     * 21004
     * 21005
     * 21006
     * 21007
     * 21008
     * 21009
     * 21010
     * 如果不是0，则说明出现错误
     */
    private Integer status;

    @Data
    public static class LatestReceiptInfoVo {
        /**
         * 客户取消交易的时间，ISO 8601格式
         * 此域仅出现在退款交易中
         */
        @JsonProperty(value = "cancellation_date")
        private String cancellationDate;
        /**
         * 客户取消交易的时间，UNIX epoch毫秒值
         * 此域仅出现在退款交易中
         */
        @JsonProperty(value = "cancellation_date_ms")
        private String cancellationDateMs;
        /**
         * 客户取消交易的时间，太平洋时区(Pacific time zone)
         * 此域仅出现在退款交易中
         */
        @JsonProperty(value = "cancellation_date_pst")
        private String cancellationDatePst;
        /**
         * 客户取消交易的原因
         * 此域仅出现在退款交易中
         */
        @JsonProperty(value = "cancellation_reason")
        private String cancellationReason;
        /**
         * 订阅到期或续订时间，ISO 8601格式
         */
        @JsonProperty(value = "expires_date")
        private String expiresDate;
        /**
         * 订阅到期或续订时间，Unix epoch毫秒值
         * <a href="https://developer.apple.com/documentation/appstorereceipts/expires_date_ms">Apple官方文档</a>
         */
        @JsonProperty(value = "expires_date_ms")
        private String expiresDateMs;
        /**
         * 订阅到期或续订时间，太平洋时区
         * 此域仅出现在退款交易中
         */
        @JsonProperty(value = "expires_date_pst")
        private String expiresDatePst;
        /**
         * 是否预览版(intro)
         * <p>
         * true - 是
         * false - 否
         */
        @JsonProperty(value = "is_in_intro_offer_period")
        private String isInIntroOfferPeriod;
        /**
         * 是否试用版(trial)
         * <p>
         * true - 是
         * false - 否
         */
        @JsonProperty(value = "is_trial_period")
        private String isTrialPeriod;
        /**
         * 标识订阅是否是升级
         * 仅出现在升级交易中
         */
        @JsonProperty(value = "is_upgraded")
        private String isUpgraded;
        /**
         * 原始购买时间，ISO 8601格式
         */
        @JsonProperty(value = "original_purchase_date")
        private String originalPurchaseDate;
        /**
         * 原始购买时间，Unix epoch毫秒值
         * 对于自动订阅消息，这个时间标识了订阅者的初始订阅时间。相同的产品类型有相同的原始订阅事件，而且在所有产品ID相同的交易中都一样。此值为原始交易的在StoreKit中的transactionDate属性。
         */
        @JsonProperty(value = "original_purchase_date_ms")
        private String originalPurchaseDateMs;
        /**
         * 原始购买时间，太平洋时区(Pacific time zone)
         */
        @JsonProperty(value = "original_purchase_date_pst")
        private String originalPurchaseDatePst;
        /**
         * 原始交易号
         */
        @JsonProperty(value = "original_transaction_id")
        private String originalTransactionId;
        /**
         * 产品编号
         * 购买的产品的唯一标识，在AppStoreConnect中创建产品时提供此值，此值与交易时SKPayment对象存储的productIdentifier相对应
         */
        @JsonProperty(value = "product_id")
        private String productId;
        /**
         * 用户赠与的信息Id
         * 见<a href="https://developer.apple.com/documentation/appstorereceipts/promotional_offer_id">Apple的官方文档</a>
         */
        @JsonProperty(value = "promotional_offer_id")
        private String promotionalOfferId;
        /**
         * AppStore上记录用户购买或恢复的时间，或AppStore上用户在一次超期后重新订阅或重新续订的时间，ISO 8601格式
         */
        @JsonProperty(value = "purchase_date")
        private String purchaseDate;
        /**
         * 对于可消费、不可消费和不可续订的订阅产品，AppStore上因购买或恢复去改变用户账户状态的时间
         * 对于自动续订的订阅产品，AppStore上因订阅或超期后续订的时间
         * UNIX epoch时间，毫秒值
         */
        @JsonProperty(value = "purchase_date_ms")
        private String purchaseDateMs;
        /**
         * AppStore上记录用户购买或恢复的时间，或AppStore上用户在一次超期后重新订阅或重新续订的时间，太平洋时区（Pacific Time Zone）
         */
        @JsonProperty(value = "purchase_date_pst")
        private String purchaseDatePst;
        /**
         * 消费产品的数量，通常为“1”，最高为“10”
         */
        @JsonProperty(value = "quantity")
        private String quantity;
        /**
         * 订阅的订阅组，见<a href="https://developer.apple.com/documentation/storekit/skproduct/2981047-subscriptiongroupidentifier">Apple的官方文档</a>
         */
        @JsonProperty(value = "subscription_group_identifier")
        private String subscriptionGroupIdentifier;
        /**
         * 交易号
         */
        @JsonProperty(value = "transaction_id")
        private String transactionId;
        /**
         * 跨设备消费事件的标识符，包括订阅续订事件，此值为订阅消费的标识值
         */
        @JsonProperty(value = "web_order_line_item_id")
        private String webOrderLineItemId;


    }

    @Data
    public static class PendingRenewalInfoVo {
        /**
         * 自动续订产品Id
         * 当前自动续订的续订信息。此值与客户订阅更新时产品的ProductIdentifier值相对应
         * 此值仅在用户降级或更改订阅时间段时出现
         */
        @JsonProperty(value = "auto_renew_product_id")
        private String autoRenewProductId;
        /**
         * 自动续订状态
         * <p>
         * 1 - 当前订阅会自动续订
         * 0 - 当前订阅不会自动续订
         */
        @JsonProperty(value = "auto_renew_status")
        private String autoRenewStatus;
        /**
         * 订阅超时的原因，此值仅在自动订阅到期时出现
         */
        @JsonProperty(value = "expiration_intent")
        private String expirationIntent;
        /**
         * 自动订阅宽限期，ISO 8601格式
         */
        @JsonProperty(value = "grace_period_expires_date")
        private String gracePeriodExpiresDate;
        /**
         * 自动订阅宽限期，UNIX epoch毫秒值，此值仅在app账单宽限期开启且用户发生账单错误时返回。
         */
        @JsonProperty(value = "grace_period_expires_date_ms")
        private String gracePeriodExpiresDateMs;
        /**
         * 自动订阅宽限期，太平洋时区(Pacific time zone)
         */
        @JsonProperty(value = "grace_period_expires_date_pst")
        private String gracePeriodExpiresDatePst;
        /**
         * 是否在账单重试期内，此值仅在自动续订订阅发生账单重试错误状态时发生
         * 见<a href="https://developer.apple.com/documentation/appstorereceipts/is_in_billing_retry_period">Apple官方说明</a>
         * <p>
         * 1 - App Store尝试重新订阅
         * 0 - App Store停止尝试重新订阅
         */
        @JsonProperty(value = "is_in_billing_retry_period")
        private String isInBillingRetryPeriod;
        /**
         * 原始付款的交易id
         * 见<a href="https://developer.apple.com/documentation/appstorereceipts/original_transaction_id">Apple的官方文档</a>
         */
        @JsonProperty(value = "original_transaction_id")
        private String originalTransactionId;
        /**
         * 订阅涨价的价格许可状态
         * 此值仅出现在用户被通知涨价时出现，默认值为0，当用户同意时，为1
         * <p>
         * 0 - 用户不同意
         * 1 - 用户同意
         */
        @JsonProperty(value = "price_consent_status")
        private String priceConsentStatus;
        /**
         * 产品编号
         * 购买的产品的唯一标识，在AppStoreConnect中创建产品时提供此值，此值与交易时SKPayment对象存储的productIdentifier相对应
         */
        @JsonProperty(value = "product_id")
        private String productId;

    }

    /**
     * 见<a href="https://developer.apple.com/documentation/appstorereceipts/responsebody/receipt">苹果官方文档</a>
     */
    @Data
    public static class ReceiptVo {
        /**
         * 见appItemId
         */
        @JsonProperty(value = "adam_id")
        private Long adamId;
        /**
         * AppStoreConnect生成，AppStore使用的唯一标识符，用于app购买。
         * 生产环境中，App有唯一的编号，请将此值视为64位整数
         */
        @JsonProperty(value = "app_item_id")
        private Long appItemId;
        /**
         * App版本号
         * 与Info.plist的CFBundleVersion(iOS)属性或CFBundleShortVersion(macOS)属性相对应
         * 生产环境中，此值和receipt_creation_date_ms时的当前版本号对应；沙盒模式时，此值永远为“1.0”
         */
        @JsonProperty(value = "application_version")
        private String applicationVersion;
        /**
         * 回执所属的bundle标识符，在App Store Connect中提供
         * 此值与App中Info.plist的CFBundleIdentifier属性相对应
         */
        @JsonProperty(value = "bundle_id")
        private String bundleId;
        /**
         * 下载的唯一序列号
         */
        @JsonProperty(value = "download_id")
        private Long downloadId;
        /**
         * 结束时间
         * 在Volume Purchase Program中App消费的到期时间，ISO 8601格式
         */
        @JsonProperty(value = "expiration_date")
        private String expirationDate;
        /**
         * 结束时间
         * 在Volume Purchase Program中App消费的到期时间，UNIX epoch毫秒值
         * 如果在Volume Purchase Program中App消费未返回此值，说明此回执没有超时时间
         */
        @JsonProperty(value = "expiration_date_ms")
        private String expirationDateMs;
        /**
         * 结束时间
         * 在Volume Purchase Program中App消费的到期时间，太平洋时区(Pacific time zone)
         */
        @JsonProperty(value = "expiration_date_pst")
        private String expirationDatePst;
        /**
         * 包含所有app内购交易的回执属性数组
         */
        @JsonProperty(value = "in_app")
        private List<InAppVo> inApp;
        /**
         * 原始App版本号
         * 与Info.plist的CFBundleVersion(iOS)属性或CFBundleShortVersion(macOS)属性相对应
         * 生产环境中，此值和receipt_creation_date_ms时的当前版本号对应；沙盒模式时，此值永远为“1.0”
         */
        @JsonProperty(value = "original_application_version")
        private String originalApplicationVersion;
        /**
         * 原始购买时间，ISO 8601格式
         */
        @JsonProperty(value = "original_purchase_date")
        private String originalPurchaseDate;
        /**
         * 原始购买时间，Unix epoch毫秒值
         * 对于自动订阅消息，这个时间标识了订阅者的初始订阅时间。相同的产品类型有相同的原始订阅事件，而且在所有产品ID相同的交易中都一样。此值为原始交易的在StoreKit中的transactionDate属性。
         */
        @JsonProperty(value = "original_purchase_date_ms")
        private String originalPurchaseDateMs;
        /**
         * 原始购买时间，太平洋时区(Pacific time zone)
         */
        @JsonProperty(value = "original_purchase_date_pst")
        private String originalPurchaseDatePst;
        /**
         * 预下单时间，ISO 8601格式
         */
        @JsonProperty(value = "preorder_date")
        private String preorderDate;
        /**
         * 预下单时间，UNIX epoch毫秒值，此值仅在用户在app上预下单时出现
         */
        @JsonProperty(value = "preorder_date_ms")
        private String preorderDateMs;
        /**
         * 预下单时间，太平洋时区（Pacific time zone）
         */
        @JsonProperty(value = "preorder_date_pst")
        private String preorderDatePst;
        /**
         * 回执创建时间，ISO 8601格式
         */
        @JsonProperty(value = "receipt_creation_date")
        private String receiptCreationDate;
        /**
         * 回执创建时间，UNIX epoch毫秒值，此值不会改变
         */
        @JsonProperty(value = "receipt_creation_date_ms")
        private String receiptCreationDateMs;
        /**
         * 回执创建时间，太平洋时区（Pacific time zone）
         */
        @JsonProperty(value = "receipt_creation_date_pst")
        private String receiptCreationDatePst;
        /**
         * 回执创建类型，此值对应app或VPP(Volume Purchase Program)消费创建时的类型
         * <p>
         * Production
         * ProductionVPP
         * ProductionSandbox
         * ProductionVPPSandbox
         */
        @JsonProperty(value = "receipt_type")
        private String receiptType;
        /**
         * 验证收据的请求时间，ISO 8601格式
         */
        @JsonProperty(value = "request_date")
        private String requestDate;
        /**
         * 验证收据的请求时间，UNIX epoch毫秒值
         */
        @JsonProperty(value = "request_date_ms")
        private String requestDateMs;
        /**
         * 验证收据的请求时间，太平洋时区(Pacific time zone)
         */
        @JsonProperty(value = "request_date_pst")
        private String requestDatePst;
        /**
         * 任意指定的APP版本信息，在沙盒模式中，此值为“0”
         */
        @JsonProperty(value = "version_external_identifier")
        private String versionExternalIdentifier;

    }

    /**
     * 见<a href="https://developer.apple.com/documentation/appstorereceipts/responsebody/receipt/in_app">Apple官方文档</a>
     */
    @Data
    public static class InAppVo {
        /**
         * 取消时间，ISO 8601格式
         * 用户取消了交易，或自动订阅计划更新的时间
         * 仅在退款交易时出现
         */
        @JsonProperty(value = "cancellation_date")
        private String cancellationDate;
        /**
         * 取消时间，Unix epoch毫秒值
         * 用户取消了交易，或自动订阅计划更新的时间
         * 仅在退款交易时出现
         * 见<a href="https://developer.apple.com/documentation/appstorereceipts/cancellation_date_ms">Apple官方文档</a>
         */
        @JsonProperty(value = "cancellation_date_ms")
        private String cancellationDateMs;
        /**
         * 取消时间，太平洋时区（Pacific time zone）
         * 用户取消了交易，或自动订阅计划更新的时间
         * 仅在退款交易时出现
         */
        @JsonProperty(value = "cancellation_date_pst")
        private String cancellationDatePst;
        /**
         * 客户取消交易的原因
         * 此域仅出现在退款交易中
         */
        @JsonProperty(value = "cancellation_reason")
        private String cancellationReason;
        /**
         * 订阅到期或续订时间，ISO 8601格式
         */
        @JsonProperty(value = "expires_date")
        private String expiresDate;
        /**
         * 订阅到期或续订时间，Unix epoch毫秒值
         * <a href="https://developer.apple.com/documentation/appstorereceipts/expires_date_ms">Apple官方文档</a>
         */
        @JsonProperty(value = "expires_date_ms")
        private String expiresDateMs;
        /**
         * 订阅到期或续订时间，太平洋时区
         * 此域仅出现在退款交易中
         */
        @JsonProperty(value = "expires_date_pst")
        private String expiresDatePst;
        /**
         * 是否预览版(intro)
         * <p>
         * true - 是
         * false - 否
         */
        @JsonProperty(value = "is_in_intro_offer_period")
        private String isInIntroOfferPeriod;
        /**
         * 是否试用版(trial)
         * <p>
         * true - 是
         * false - 否
         */
        @JsonProperty(value = "is_trial_period")
        private String isTrialPeriod;
        /**
         * 原始购买时间，ISO 8601格式
         */
        @JsonProperty(value = "original_purchase_date")
        private String originalPurchaseDate;
        /**
         * 原始购买时间，Unix epoch毫秒值
         * 对于自动订阅消息，这个时间标识了订阅者的初始订阅时间。相同的产品类型有相同的原始订阅事件，而且在所有产品ID相同的交易中都一样。此值为原始交易的在StoreKit中的transactionDate属性。
         */
        @JsonProperty(value = "original_purchase_date_ms")
        private String originalPurchaseDateMs;
        /**
         * 原始购买时间，太平洋时区(Pacific time zone)
         */
        @JsonProperty(value = "original_purchase_date_pst")
        private String originalPurchaseDatePst;
        /**
         * 原始交易号
         */
        @JsonProperty(value = "original_transaction_id")
        private String originalTransactionId;
        /**
         * 产品编号
         * 购买的产品的唯一标识，在AppStoreConnect中创建产品时提供此值，此值与交易时SKPayment对象存储的productIdentifier相对应
         */
        @JsonProperty(value = "product_id")
        private String productId;
        /**
         * 用户赠与的信息Id
         * 见<a href="https://developer.apple.com/documentation/appstorereceipts/promotional_offer_id">Apple的官方文档</a>
         */
        @JsonProperty(value = "promotional_offer_id")
        private String promotionalOfferId;
        /**
         * AppStore上记录用户购买或恢复的时间，或AppStore上用户在一次超期后重新订阅或重新续订的时间，ISO 8601格式
         */
        @JsonProperty(value = "purchase_date")
        private String purchaseDate;
        /**
         * 对于可消费、不可消费和不可续订的订阅产品，AppStore上因购买或恢复去改变用户账户状态的时间
         * 对于自动续订的订阅产品，AppStore上因订阅或超期后续订的时间
         * UNIX epoch时间，毫秒值
         */
        @JsonProperty(value = "purchase_date_ms")
        private String purchaseDateMs;
        /**
         * AppStore上记录用户购买或恢复的时间，或AppStore上用户在一次超期后重新订阅或重新续订的时间，太平洋时区（Pacific Time Zone）
         */
        @JsonProperty(value = "purchase_date_pst")
        private String purchaseDatePst;
        /**
         * 消费产品的数量，通常为“1”，最高为“10”
         */
        @JsonProperty(value = "quantity")
        private String quantity;
        /**
         * 交易号
         */
        @JsonProperty(value = "transaction_id")
        private String transactionId;
        /**
         * 跨设备消费事件的标识符，包括订阅续订事件，此值为订阅消费的标识值
         */
        @JsonProperty(value = "web_order_line_item_id")
        private String webOrderLineItemId;

    }
}
