package cn.org.wangchangjiu.pay.apple.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Classname AppleNotificationRequest
 * @Description
 * @Date 2022/12/12 11:29
 * @Created by wangchangjiu
 */
@Data
public class AppleNotificationRequest implements Serializable {

    /**
     * 内购产品的数字序号
     */
    @JsonProperty(value = "auto_renew_adam_id")
    private String autoRenewAdamId;

    /**
     * 内购产品的id
     */
    @JsonProperty(value = "auto_renew_product_id")
    private String autoRenewProductId;

    /**
     * 自动订阅状态
     */
    @JsonProperty(value = "auto_renew_status")
    private String autoRenewStatus;

    /**
     * 自动订阅状态变化日期
     */
    @JsonProperty(value = "auto_renew_status_change_date")
    private String autoRenewStatusChangeDate;

    /**
     * 自动订阅状态变化日期（毫秒值）
     */
    @JsonProperty(value = "auto_renew_status_change_date_ms")
    private String autoRenewStatusChangeDateMs;

    /**
     * 自动订阅状态变化日期（PST时区，Pacific time zone）
     */
    @JsonProperty(value = "auto_renew_status_change_date_pst")
    private String autoRenewStatusChangeDatePst;

    /**
     * <p>环境</p>
     * <ul>
     * <li>Sandbox 沙盒环境</li>
     * <li>PROD 生产环境</li>
     * </ul>
     */
    @JsonProperty(value = "environment")
    private String environment;

    /**
     * 停止订阅的原因选项，见<a href="https://developer.apple.com/documentation/appstorereceipts/expiration_intent">Apple的说明</a>
     * <ul>
     * <li>1 - 客户取消</li>
     * <li>2 - 订单错误，如客户支付信息不正确</li>
     * <li>3 - 客户不同意最近的价格提升</li>
     * <li>4 - 续订时产品无效</li>
     * <li>5 - 其它</li>
     * </ul>
     */
    @JsonProperty(value = "expiration_intent")
    private Integer expirationIntent;

    /**
     * 最新的base64编码的交易收据。
     */
    @JsonProperty(value = "latest_receipt")
    private String latestReceipt;

    /**
     * 最新的base64编码的交易收据。
     */
    @JsonProperty(value = "latest_receipt_info")
    private LatestReceiptInfoModel latestReceiptInfo;

    /**
     * 通知类型
     */
    @JsonProperty(value = "notification_type")
    private String notificationType;

    /**
     * 与验证收据请求相同的值
     */
    @JsonProperty(value = "password")
    private String password;

    /**
     * 统一订单，包含近期内购交易的信息
     */
    @JsonProperty(value = "unified_receipt")
    private UnifiedReceiptModel unifiedReceipt;


    /**
     *  包名
     */
    @JsonProperty(value = "bid")
    private String bid;

    /**
     * 见<a href="https://developer.apple.com/documentation/appstorereceipts/responsebody/latest_receipt_info">苹果官方文档</a>
     */
    @Data
    public static class LatestReceiptInfoModel {

        @JsonProperty(value = "cancellation_date")
        private String cancellationDate;
        @JsonProperty(value = "cancellation_date_ms")
        private String cancellationDateMs;
        @JsonProperty(value = "cancellation_date_pst")
        private String cancellationDatePst;
        @JsonProperty(value = "cancellation_reason")
        private String cancellationReason;

        /**
         * 订阅到期或将开始续订的时间，epoch毫秒值
         */
        @JsonProperty(value = "expires_date")
        private String expiresDate;
        @JsonProperty(value = "expires_date_ms")
        private String expiresDateMs;
        @JsonProperty(value = "expires_date_pst")
        private String expiresDatePst;

        /**
         * 是否预览版(intro)
         * <ul>
         * <li>true</li>
         * <li>false</li>
         * </ul>
         */
        @JsonProperty(value = "is_in_intro_offer_period")
        private String isInIntroOfferPeriod;

        /**
         * 是否试用版(trial)
         * <ul>
         * <li>true</li>
         * <li>false</li>
         * </ul>
         */
        @JsonProperty(value = "is_trial_period")
        private String isTrialPeriod;
        @JsonProperty(value = "is_upgraded")
        private String isUpgraded;

        /**
         * 原始支付时间，ISO 8601格式
         */
        @JsonProperty(value = "original_purchase_date")
        private String originalPurchaseDate;

        /**
         * 原始支付时间，epoch毫秒值
         */
        @JsonProperty(value = "original_purchase_date_ms")
        private String originalPurchaseDateMs;

        /**
         * 原始支付时间，pst时区（Pacific time zone}）
         */
        @JsonProperty(value = "original_purchase_date_pst")
        private String originalPurchaseDatePst;

        /**
         * 原始交易凭证
         */
        @JsonProperty(value = "original_transaction_id")
        private String originalTransactionId;

        /**
         * 商品id
         */
        @JsonProperty(value = "product_id")
        private String productId;
        @JsonProperty(value = "promotional_offer_id")
        private String promotionalOfferId;

        /**
         * 购买时间，ISO 8601格式
         */
        @JsonProperty(value = "purchase_date")
        private String purchaseDate;

        /**
         * 购买时间，epoch毫秒值
         */
        @JsonProperty(value = "purchase_date_ms")
        private String purchaseDateMs;

        /**
         * 购买时间，pst时区(Pacific time zone)
         */
        @JsonProperty(value = "purchase_date_pst")
        private String purchaseDatePst;

        /**
         * 产品购买的数量，此值通常为“1”，最大值为“10”
         */
        @JsonProperty(value = "quantity")
        private String quantity;
        @JsonProperty(value = "subscription_group_identifier")
        private String subscriptionGroupIdentifier;

        /**
         * 某次付款、恢复、续订的唯一标识码
         */
        @JsonProperty(value = "transaction_id")
        private String transactionId;

        /**
         * 跨设备付款的唯一标识符，包括订购刷新事件在内。此值是标识定够付款的主键。
         */
        @JsonProperty(value = "web_order_line_item_id")
        private String webOrderLineItemId;


    }

    @Data
    public static class UnifiedReceiptModel {
        /**
         * 环境
         * <ul>
         * <li>Sandbox - 沙盒模式</li>
         * <li></li>
         * </ul>
         */
        @JsonProperty(value = "environment")
        private String environment;

        /**
         * 最新的App收据，base64编码
         */
        @JsonProperty(value = "latest_receipt")
        private String latestReceipt;

        /**
         * 最近100条收据信息，其中不包含已完成的
         */
        @JsonProperty(value = "latest_receipt_info")
        private List<LatestReceiptInfoModel> latestReceiptInfo;

        /**
         * 含自动续订订阅信息中未确认的续订
         */
        @JsonProperty(value = "pending_renewal_info")
        private List<PendingRenewalInfoModel> pendingRenewalInfo;

        /**
         * 状态码，0表示正常
         */
        @JsonProperty(value = "status")
        private Integer status;


    }

    /**
     * 查看 <a href="https://developer.apple.com/documentation/appstorereceipts/responsebody/pending_renewal_info">Apple的说明</a>
     */
    @Data
    public static class PendingRenewalInfoModel {
        /**
         * 自动续订的产品id
         */
        @JsonProperty(value = "auto_renew_product_id")
        private String autoRenewProductId;
        /**
         * 自动续订状态
         * <ul>
         * <li>1 - 到期后续订</li>
         * <li>0 - 到期后不续订</li>
         * </ul>
         */
        @JsonProperty(value = "auto_renew_status")
        private String autoRenewStatus;
        /**
         * 停止订阅的原因选项，见<a href="https://developer.apple.com/documentation/appstorereceipts/expiration_intent">Apple的说明</a>
         * <ul>
         * <li>1 - 客户取消</li>
         * <li>2 - 订单错误，如客户支付信息不正确</li>
         * <li>3 - 客户不同意最近的价格提升</li>
         * <li>4 - 续订时产品无效</li>
         * <li>5 - 其它</li>
         * </ul>
         */
        @JsonProperty(value = "expiration_intent")
        private String expirationIntent;
        /**
         * 订阅宽限期超时时间，ISO 8601格式
         */
        @JsonProperty(value = "grace_period_expires_date")
        private String gracePeriodExpiresDate;
        /**
         * 订阅宽限期超时时间，毫秒值
         */
        @JsonProperty(value = "grace_period_expires_date_ms")
        private String gracePeriodExpiresDateMs;
        /**
         * 订阅宽限期超时时间，pst时区
         */
        @JsonProperty(value = "grace_period_expires_date_pst")
        private String gracePeriodExpiresDatePst;
        /**
         * 标识苹果是否尝试自动更新已过期的订阅，此标识只在自动续订的订阅处于账单重试状态下出现
         * <ul>
         * <li>1 - App store正在尝试续订</li>
         * <li>0 - App store已停止尝试续订</li>
         * </ul>
         */
        @JsonProperty(value = "is_in_billing_retry_period")
        private String isInBillingRetryPeriod;

        /**
         * 原始交易凭证
         */
        @JsonProperty(value = "original_transaction_id")
        private String originalTransactionId;
        /**
         * 客户是否同意价格变动的标识
         * <ul>
         * <li>1 - 同意</li>
         * <li>0 - 不同意</li>
         * </ul>
         */
        @JsonProperty(value = "price_consent_status")
        private String priceConsentStatus;

        /**
         * 产品id
         */
        @JsonProperty(value = "product_id")
        private String productId;

    }

    public boolean hasValid(){
        return this.getUnifiedReceipt().getStatus() == 0;
    }

}
