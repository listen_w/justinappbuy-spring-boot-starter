package cn.org.wangchangjiu.pay.apple.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Classname ApplePayVerifyRequest
 * @Description
 * @Date 2023/6/19 20:56
 * @Created by wangchangjiu
 */
@Data
public class ApplePayVerifyRequest implements Serializable {

    private String receiptData;

    private String areaCode;

}
