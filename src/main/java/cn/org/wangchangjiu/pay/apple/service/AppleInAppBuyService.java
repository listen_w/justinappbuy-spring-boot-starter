package cn.org.wangchangjiu.pay.apple.service;

import cn.org.wangchangjiu.pay.apple.request.AppleNotificationRequest;
import cn.org.wangchangjiu.pay.apple.request.ApplePayVerifyRequest;
import cn.org.wangchangjiu.pay.apple.vo.AppleVerifyReceiptVo;
import cn.org.wangchangjiu.pay.common.InAppBuyService;
import cn.org.wangchangjiu.pay.common.VerifyResult;
import cn.org.wangchangjiu.pay.common.dto.VipOrder;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static cn.org.wangchangjiu.pay.apple.AppleStatusConstant.*;

/**
 * @Classname AppleInAppBuyService
 * @Description
 * @Date 2023/6/19 20:36
 * @Created by wangchangjiu
 */
public class AppleInAppBuyService implements InAppBuyService<ApplePayVerifyRequest, AppleNotificationRequest> {

    private static final Logger log = LoggerFactory.getLogger(AppleInAppBuyService.class);

    @Override
    public VerifyResult verify(ApplePayVerifyRequest inAppVerifyRequest, Map<String, Object> params) {
        AppleVerifyReceiptVo receiptVo = verifyReceipt(inAppVerifyRequest.getReceiptData(), inAppVerifyRequest.getSandBox(), inAppVerifyRequest.getClientType());
        VerifyReceiptVO verifyReceiptVO;
        Integer status = receiptVo.getStatus();

        switch (status) {
            case APPLE_STATUS_SUCCESS:
                //沙箱支付且沙箱支付关闭  如果沙盒环境关闭,则产生新的收据,但是不产生订单,更不发放会员
                if (isSandBox && !applePayProperties.getUseSandBox()) {
                    log.info("the sandbox payment is closed. Transaction information is generated, no order is generated, and no member is issued");
                    dealReceiptService.dealSandBoxReceiptNotOrder(request.getReceiptData(), receiptVo);
                    verifyReceiptVO = VerifyReceiptVO.buildNotAllowStatus();
                    break;
                }
                //创建苹果消费单,创建订单
                verifyReceiptVO = dealSuccessReceipt(inAppVerifyRequest.getReceiptData(), receiptVo, requestHeader);
                break;
            case APPLE_STATUS_21007:
                //此收据来自测试环境，但它被发送到生产环境进行验证。
                //现在重新发送到沙箱环境
                log.info("receipt from sandbox,but sent prd,server will redirect to sandbox");
                return verifyAppleReceipt(request, true, requestHeader);
            case APPLE_STATUS_21002:
                dealReceiptService.dealExceptionReceipt(request, receiptVo, requestHeader, APPLE_STATUS_21002,  isSandBox);
                verifyReceiptVO = VerifyReceiptVO.buildRetryStatus(APPLE_STATUS_21002);
                break;
            case APPLE_STATUS_21005:
                dealReceiptService.dealExceptionReceipt(request, receiptVo, requestHeader, APPLE_STATUS_21005,  isSandBox);
                verifyReceiptVO = VerifyReceiptVO.buildRetryStatus(APPLE_STATUS_21005);
                break;
            case APPLE_STATUS_21009:
                dealReceiptService.dealExceptionReceipt(request, receiptVo, requestHeader, APPLE_STATUS_21009, isSandBox);
                verifyReceiptVO = VerifyReceiptVO.buildRetryStatus(APPLE_STATUS_21009);
                break;
            default:
                //其他响应码,请求错误
                log.error("verify receipt error , receiptData : {} , response : {}", request.getReceiptData(), receiptVo);
                verifyReceiptVO =  VerifyReceiptVO.buildErrorStatus(status);
        }
        return verifyReceiptVO;
    }

    private AppleVerifyReceiptVo verifyReceipt(String receiptData, Boolean isSandBox, String clientType) {
        String password = applePayProperties.getPasswordMap().get(clientType);
        if (StringUtils.isBlank(password)){
           // throw new (NOT_SUPPORT_APPLE_SIGN);
        }
        JSONObject request = new JSONObject();
        request.put("receipt-data", receiptData);
        request.put("password", password);
        return isSandBox ? appleSandBoxServiceFeign.verifyReceipt(request) : applePrdServiceFeign.verifyReceipt(request);
    }

    @Override
    public void subscribeNotify(AppleNotificationRequest appleNotificationRequest) {

    }

    @Override
    public void applyRefund(VipOrder vipOrder) {

    }
}
