package cn.org.wangchangjiu.pay.apple;

/**
 * @Classname AppleStatusConstant
 * @Description  苹果校验收据返回状态码
 *
 *  https://developer.apple.com/documentation/appstorereceipts/status
 * @Date 2022/12/12 13:55
 * @Created by wangchangjiu
 */
public final class AppleStatusConstant {

    /**
     * 苹果响应-success
     */
    public static final int APPLE_STATUS_SUCCESS = 0;

    /**
     * 苹果响应-接收数据属性中的数据格式错误或服务遇到临时问题。再试一次。
     */
    public static final int APPLE_STATUS_21002 = 21002;

    /**
     * 苹果响应-收据服务器暂时无法提供收据。再试一次。
     */
    public static final int APPLE_STATUS_21005 = 21005;

    /**
     * 此收据来自测试环境，但它被发送到生产环境进行验证。
     */
    public static final int APPLE_STATUS_21007 = 21007;

    /**
     * 苹果响应-内部数据访问错误。稍后再试。
     */
    public static final int APPLE_STATUS_21009 = 21009;

}
